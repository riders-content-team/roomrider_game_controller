#!/usr/bin/env python

import rospy
from std_msgs.msg import String
import json
from std_srvs.srv import Trigger, TriggerResponse
from gazebo_msgs.srv import GetModelState
import time
import os
import requests

class GameController:

    def __init__(self, robot_name):

        self.robot_name = robot_name

        self.robot_status = "Paused"

        self.purple_trash_collected = 0
        self.blue_trash_collected = 0

        self.metrics = {
            'Purple Trash Collected' : self.purple_trash_collected,
            'Blue Trash Collected' : self.blue_trash_collected,
        }

        rospy.init_node("game_controller")

        self.metric_pub = rospy.Publisher('simulation_metrics', String, queue_size=10)

        rospy.Subscriber("collision", String, self.collision_cb)

        s = rospy.Service('/robot_handler', Trigger, self.handle_robot)

        try:
            rospy.wait_for_service("/gazebo/get_model_state", 1.0)
            robot_check = rospy.ServiceProxy("/gazebo/get_model_state", GetModelState)

            resp = robot_check("roomrider", "")
            if not resp.success == True:
                print("no robot around")
                self.robot_status = "no_robot"

        except (rospy.ServiceException, rospy.ROSException), e:
            self.robot_status = "no_robot"

            rospy.logerr("Service call failed: %s" % (e,))

        rate = rospy.Rate(10)

        while ~rospy.is_shutdown():
            if self.purple_trash_collected == 12:
                self.achieve(35)
            if self.purple_trash_collected == 4:
                self.achieve(43)
            if self.blue_trash_collected == 4:
                self.achieve(36)
            if self.purple_trash_collected >= 12 and self.blue_trash_collected >= 4:
                self.achieve(37)
            if self.purple_trash_collected == 36 and self.blue_trash_collected == 12:
                self.achieve(38)
            if self.purple_trash_collected == 36:
                self.achieve(40)
            if self.blue_trash_collected == 12:
                self.achieve(42)
            self.publish_metrics()
            rate.sleep()

    def handle_robot(self,req):
        if self.robot_status == "Paused":
            self.robot_status = "Running"
            return TriggerResponse(
                success = True,
                message = self.robot_status
            )

        elif self.robot_status == "Running":
            return TriggerResponse(
                success = True,
                message = self.robot_status
            )

        elif self.robot_status == "Stop":
            return TriggerResponse(
                success = True,
                message = self.robot_status
            )

        elif self.robot_status == "no_robot":
            return TriggerResponse(
                success = True,
                message = self.robot_status
            )

    def publish_metrics(self):
        self.metrics['Status'] = str(self.robot_status)
        self.metrics['Purple Trash Collected'] = self.purple_trash_collected
        self.metrics['Blue Trash Collected'] = self.blue_trash_collected
        self.metric_pub.publish(json.dumps(self.metrics, sort_keys=True))

    def collision_cb(self, msg):
        if "blue" in msg.data:
            self.blue_trash_collected += 1
        if "pink" in msg.data:
            self.purple_trash_collected += 1

    def achieve(self, achievement_id):
        # get parameters from environment
        host = os.environ.get('RIDERS_HOST', None)
        project_id = os.environ.get('RIDERS_PROJECT_ID', None)
        token = os.environ.get('RIDERS_AUTH_TOKEN', None)

        # generate request url
        achievement_url = '%s/api/v1/project/%s/achievements/%s/achieve/' % (host, project_id, achievement_id)

        # send request
        rqst = requests.post(achievement_url, headers={
            'Authorization': 'Token %s' % token,
            'Content-Type': 'application/json',
        })


if __name__ == '__main__':
    # Name of robot
    controller = GameController("roomrider")
